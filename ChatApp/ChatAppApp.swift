//
//  ChatAppApp.swift
//  ChatApp
//
//  Created by cis on 30/11/22.
//

import SwiftUI

@main
struct ChatAppApp: App {
    var body: some Scene {
        WindowGroup {
            MainMessagesView()
        }
    }
}
